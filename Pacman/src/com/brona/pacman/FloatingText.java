package com.brona.pacman;

import com.brona.gameengine.Input;
import com.brona.gameengine.Renderer;
import com.brona.gameengine.renderables.Text;

public class FloatingText extends GameObject {

	public static final int TEXT_LAYER = 10;
	public static final float SPEED = 20f;
	
	private int alpha;
	private Text text;
	private int color;
	
	public FloatingText(float positionX, float positionY, String text, int color) {
		super(positionX, positionY);
		
		this.color = color & 0x00ffffff;
		alpha = 0;
		
		this.text = new Text(text);
	}

	@Override
	public void update(Input input, float deltaTime) {
		positionY -= deltaTime * SPEED;
		
		alpha += deltaTime * 255;
		if(alpha >= 255) {
			dead = true;
			return;
		}
	
	    text.setColor(color | (alpha << 24) );
	}

	@Override
	public void render(Renderer renderer) {
		renderer.add(text, ((int)super.positionX) - this.text.getWidth() / 2, ((int)super.positionY) - this.text.getHeight() / 2, TEXT_LAYER);

	}

}
