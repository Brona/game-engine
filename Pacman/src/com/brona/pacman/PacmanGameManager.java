package com.brona.pacman;

import java.util.ArrayList;
import java.util.Random;

import com.brona.gameengine.Engine;
import com.brona.gameengine.GameInterface;
import com.brona.gameengine.Input;
import com.brona.gameengine.Renderer;
import com.brona.gameengine.renderables.Image;
import com.brona.gameengine.renderables.Rectangle;
import com.brona.gameengine.renderables.Text;

public class PacmanGameManager implements GameInterface {

	public static final int TILE_SIZE = 16;
	public static final int WIDTH_IN_TILES = 21;
	public static final int HEIGHT_IN_TILES = 15;
	public static final int POSITION_RECTANGLE_LAYER = 2;
	
	private Engine engine;
	
	private int mode = 0;
	private boolean debug = false;
	
	private Pacman pacman;
	private ArrayList<Ghost> ghosts;
	private ArrayList<SpecialPoint> specials;
	private ArrayList<FloatingText> texts;
	private Text fps, remaining, score;
	private Text statusText;
	private Rectangle path, wall;
	private Image point;
	
	private boolean[][] walkable, points;
	private int maxPoints;
	private int pointsCount;
	private int scoreCount;
	
	public PacmanGameManager() {
		this.walkable = new boolean[WIDTH_IN_TILES][HEIGHT_IN_TILES];
		this.points   = new boolean[WIDTH_IN_TILES][HEIGHT_IN_TILES];
		this.ghosts   = new ArrayList<>();
		this.specials = new ArrayList<>();
		this.texts    = new ArrayList<>();
	}
	
	@Override
	public void init(Engine engine) {
		this.engine = engine;
		
		engine.getWindow().setIcon("/pacman/icon_ghost.png");
		
		this.fps = new Text("", java.awt.Color.CYAN.getRGB());
		this.remaining = new Text("", java.awt.Color.WHITE.getRGB());
		this.score = new Text("", java.awt.Color.WHITE.getRGB());
		this.statusText = new Text("", java.awt.Color.WHITE.getRGB());
		
		this.point = new Image("/pacman/images/point.png", true);
		this.path = new Rectangle(TILE_SIZE, TILE_SIZE, java.awt.Color.GRAY.getRGB(), true);
		this.wall = new Rectangle(TILE_SIZE, TILE_SIZE, java.awt.Color.BLACK.getRGB(), true);
		
		generate();
	}

	private void generate() {
		Random pacmanR = new Random();
		this.pacman = new Pacman((pacmanR.nextInt(WIDTH_IN_TILES / 2 - 1) * 2 + 1) * TILE_SIZE + TILE_SIZE/2, (pacmanR.nextInt(HEIGHT_IN_TILES / 2 - 1) * 2 + 1) * TILE_SIZE + TILE_SIZE/2); pacman.init(engine, this);
    	
    	this.scoreCount = 0;
    	this.mode = 0;
    	this.ghosts.clear();
    	this.specials.clear();
    	
    	this.addGhost();
    	this.addGhost();
    	
    	Random r = new Random();
    	this.pointsCount = 0;
		
		for(int y = 0; y < HEIGHT_IN_TILES; ++y) {
        	for(int x = 0; x < WIDTH_IN_TILES; ++x) {
        	    if((x == 0) || (y == 0) || (x == WIDTH_IN_TILES -1) || (y == HEIGHT_IN_TILES -1)) {
        	    	this.walkable[x][y] = false; this.points[x][y] = false;
        	    } else if((x == this.pacman.getTileX()) && (y == this.pacman.getTileY())) {
        	    	this.walkable[x][y] = true; this.points[x][y] = false;
        	    } else if((x % 2 == 0) && (y % 2 == 0)) {
        	    	this.walkable[x][y] = false; this.points[x][y] = false;
        	    } else {
        	    	if(r.nextInt(33) < 32) {
        	    		this.walkable[x][y] = true; this.points[x][y] = true; this.pointsCount++;
        	    	} else {
        	    		this.walkable[x][y] = false; this.points[x][y] = false;
        	    	}
        	    }	
        	}
        }
		
		addSpecialPoint();
		
		for(Ghost ghost : ghosts) {
			int x = ghost.getTileX(), y = ghost.getTileY();
			if(!this.walkable[x][y])
				this.walkable[x][y] = true;
			if(!this.points[x][y])
				{ this.points[x][y] = true; this.pointsCount++; }
		}
		
		maxPoints = pointsCount;
	}
	
	@Override
	public void update(Input input, float deltaTime) {
		this.fps.setText("=DEBUG (F1)= FPS: " + Math.round(1 / deltaTime));
        this.pacman.update(input, deltaTime);
        for(Ghost ghost : ghosts) {
        	ghost.update(input, deltaTime);
        }
        
        for(int i = 0; i < specials.size(); ++i) {
			specials.get(i).update(input, deltaTime);
			if(specials.get(i).isDead()) {
				specials.remove(i);
				--i;
			}
		}
        
        for(int i = 0; i < texts.size(); ++i) {
			texts.get(i).update(input, deltaTime);
			if(texts.get(i).isDead()) {
				texts.remove(i);
				--i;
			}
		}
        
	    if(engine.getInput().isKeyDown(java.awt.event.KeyEvent.VK_ESCAPE)) {
	    	switch(mode) {
	    	case 2: mode = 4; break;
	    	case 3: mode = 5; break;
	    	case 4: mode = 2; break;
	    	case 5: mode = 3; break;
	    	default: break;
	    	}
	    }
	
	    if(engine.getInput().isKeyDown(java.awt.event.KeyEvent.VK_SPACE)) {
	    	generate();
	    }
	
	    if((maxPoints - this.pointsCount) / 80 > this.ghosts.size() - 2) {
	        this.addGhost();
	        addSpecialPoint();
	    }
	    
	    if(this.pointsCount == 0) {
	    	this.mode = (mode % 2 == 0) ? 6 : 7;
	    }
	    
	    switch(mode) {
	    case 0:
	    case 1:
	    	if(this.statusText.getText() != "PRESS ANY ARROW OR WASD TO START THE GAME")
	    	    this.statusText.setText("PRESS ANY ARROW OR WASD TO START THE GAME");
	    	break;
	    case 4:
	    case 5:
	    	if(this.statusText.getText() != "GAME IS PAUSED. PRESS ESC OR ANY ARROW OR WASD TO RESUME")
	    	    this.statusText.setText("GAME IS PAUSED. PRESS ESC OR ANY ARROW OR WASD TO RESUME");
	    	break;
	    case 6:
	    case 7:
	    	if(this.pointsCount == 0) {
	    		if(this.statusText.getText() != "YOU WON. PRESS SPACE TO RESTART")
		    	    this.statusText.setText("YOU WON. PRESS SPACE TO RESTART");
		    } else {
	    	    if(this.statusText.getText() != "YOU LOST. PRESS SPACE TO RESTART")
	    	        this.statusText.setText("YOU LOST. PRESS SPACE TO RESTART");
		    }
	    	break;
	    default:
	    	if(this.statusText.getText() != "GAME IS RUNNING")
	    	    this.statusText.setText("GAME IS RUNNING");
	    	break;
	    }
	    
	    if(input.isKeyDown(java.awt.event.KeyEvent.VK_F1)) {
	    	debug = !debug;
	    }
	    
	}

	private void addGhost() {
		Random r = new Random();
		Ghost g = new Ghost((r.nextInt(WIDTH_IN_TILES / 2 - 1) * 2 + 1) * TILE_SIZE + TILE_SIZE/2, (r.nextInt(HEIGHT_IN_TILES / 2 - 1) * 2 + 1) * TILE_SIZE + TILE_SIZE/2);
		g.init(engine, this);
		ghosts.add(g);
	}
	
	private void addSpecialPoint() {
		Random r = new Random();
		SpecialPoint sp = new SpecialPoint((r.nextInt(WIDTH_IN_TILES / 2 - 1) * 2 + 1) * TILE_SIZE + TILE_SIZE/2, (r.nextInt(HEIGHT_IN_TILES / 2 - 1) * 2 + 1) * TILE_SIZE + TILE_SIZE/2);
		sp.init(engine, this);
		if(points[sp.getTileX()][sp.getTileY()]) {
			points[sp.getTileX()][sp.getTileY()] = false;
		    --pointsCount; //TODO Check for not subtracting
		}
		
		if(!walkable[sp.getTileX()][sp.getTileY()]) {
			walkable[sp.getTileX()][sp.getTileY()] = true;
		}
		
		specials.add(sp);
	}
	
	@Override
	public void render(Renderer renderer) {
		if(debug) renderer.addStatical(this.fps, engine.getWidth() / 2 - fps.getWidth() / 2, TILE_SIZE / 2 - this.fps.getHeight() / 2, 5);
		
		this.remaining.setText("Remaining: " + this.pointsCount);
		renderer.addStatical(this.remaining, TILE_SIZE / 2, TILE_SIZE / 2 - this.remaining.getHeight() / 2, 5);
		
		this.score.setText("Score: " + this.scoreCount);
		renderer.addStatical(this.score, (WIDTH_IN_TILES - 1) * TILE_SIZE + TILE_SIZE / 2 - this.score.getWidth(), TILE_SIZE / 2 - this.score.getHeight() / 2, 5);
		
		renderer.addStatical(this.statusText, engine.getWidth() / 2 - this.statusText.getWidth() / 2, (HEIGHT_IN_TILES - 1) * TILE_SIZE + TILE_SIZE / 2 - this.statusText.getHeight() / 2, FloatingText.TEXT_LAYER);
		
        this.pacman.render(renderer);
        for(Ghost ghost : ghosts) {
        	ghost.render(renderer);
        }
        for(SpecialPoint sp : specials) {
        	sp.render(renderer);
        }
        for(FloatingText f : texts) {
        	f.render(renderer);
        }
        
        for(int y = 0; y < HEIGHT_IN_TILES; ++y) {
        	for(int x = 0; x < WIDTH_IN_TILES; ++x) {
        		if(this.walkable[x][y]) {
        		    renderer.add(this.path, x * TILE_SIZE, y * TILE_SIZE, 1);	
        		} else {
        			renderer.add(this.wall, x * TILE_SIZE, y * TILE_SIZE, 1);	
        		}
            }
        }
        
        for(int y = 0; y < HEIGHT_IN_TILES; ++y) {
        	for(int x = 0; x < WIDTH_IN_TILES; ++x) {
        		if(this.points[x][y]) {
        		    renderer.add(this.point, x * TILE_SIZE + (TILE_SIZE - this.point.getWidth()) / 2, y * TILE_SIZE + (TILE_SIZE - this.point.getHeight()) / 2, SpecialPoint.POINT_LAYER);	
        		}
            }
        }
	}

	public static void main(String[] args) {
		Engine engine = new Engine(new PacmanGameManager());
		engine.setWidth(WIDTH_IN_TILES * TILE_SIZE);
		engine.setHeight(HEIGHT_IN_TILES * TILE_SIZE);
		engine.setScale(3f);
		engine.setTitle("Pacman Pre-alpha v7");
		engine.setFps(60);
		engine.start();
	}

	public Pacman getPacman() {
		return this.pacman;
	}

	public int getMode() {
		return this.mode;
	}

	public void setMode(int mode) {
		this.mode = mode;
	}

	public boolean[][] getWalkable() {
		return this.walkable;
	}

	public boolean[][] getPoints() {
		return this.points;
	}

	public int getPointsCount() {
		return this.pointsCount;
	}

	public void setPointsCount(int pointsCount) {
		this.pointsCount = pointsCount;
	}

	public int getScoreCount() {
		return this.scoreCount;
	}

	public void setScoreCount(int scoreCount) {
		this.scoreCount = scoreCount;
	}

	public void addFloatingText(FloatingText f) {
        texts.add(f);
	}

	boolean isDebug() {
		return debug;
	}
}
