package com.brona.samplegame;

import com.brona.gameengine.*;
import com.brona.gameengine.Renderer.*;
import com.brona.gameengine.renderables.*;

public class SampleGame implements GameInterface {

	private Image wood;
	private Animation pointer;
	private Text text1, text2, text3, text4, text5;
	private Rectangle rec1, rec2, rect;
	private Camera camera;
	
	public SampleGame() {
		this.wood = new Image("/images/wood.png", true);
		this.rect = new Rectangle(16, 16, java.awt.Color.YELLOW.getRGB(), true);
		this.pointer = new Animation("/images/pointer_rev2.png", true, 0.12f);
		
		this.text1 = new Text("", java.awt.Color.CYAN.getRGB());
		this.text2 = new Text("", java.awt.Color.CYAN.getRGB());
		this.text3 = new Text("Pacman facing: RIGHT", java.awt.Color.CYAN.getRGB());
		this.text4 = new Text("Creator: brona.ruzicka@gmail.com", java.awt.Color.CYAN.getRGB());
		this.text5 = new Text(" !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSRTUVWXYZ^", java.awt.Color.RED.getRGB());
		
		this.rec1 = new Rectangle(10, 50, java.awt.Color.RED.getRGB(), true);
		this.rec2 = new Rectangle( 8, 48, java.awt.Color.GREEN.getRGB(), false);
		
		this.camera = new MovingCamera(0, 0);
	}

	public void init(Engine engine) {
		this.camera.init(engine, this);
	}	
	
	public void update(Engine engine, float deltaTime, int fps) {		
        this.text1.setText("FPS:" + fps);
		this.text2.setText("X: " + this.camera.getOffsetX() + " Y: " + this.camera.getOffsetY());
		
		this.pointer.update(deltaTime);
		
		this.camera.update(engine, this, deltaTime);	
	}

	public void render(Engine engine, Renderer renderer) {

		renderer.addStatical(this.text1, 1, 1, Renderer.UPPERMOST_LAYER);
		renderer.addStatical(this.text2, 1, 8, Renderer.UPPERMOST_LAYER);
		renderer.addStatical(this.text3, 1, 15, Renderer.UPPERMOST_LAYER);
		renderer.addStatical(this.text4, 1, 22, Renderer.UPPERMOST_LAYER);
		renderer.add(this.text5, 160, 120, Renderer.UPPERMOST_LAYER - 2);

		renderer.addStatical(rect, engine.getWidth() / 2 - this.rect.getWidth() / 2,
				engine.getHeight() / 2 - this.rect.getHeight() / 2, Renderer.UPPERMOST_LAYER -1);
		
		renderer.addStatical(this.pointer, engine.getInput().getMouseX() - this.pointer.getWidth() / 2,
				engine.getInput().getMouseY() - this.pointer.getHeight() / 2, Renderer.UPPERMOST_LAYER -1);
		
		renderer.add(this.wood.setIsTransparent(true), 60, 60, 1);
        renderer.add(this.wood.setIsTransparent(false), 60, 150, 1);

        renderer.add(this.rec1, 200, 10, 1);
        renderer.add(this.rec2, 201, 11, 2);
	}

	public static void main(String[] args) {
		Engine engine = new Engine(new SampleGame());
		engine.setWidth(320);
		engine.setHeight(240);
		engine.setScale(3f);
		engine.setTitle("Brona's OpenGE Sample Pre-alpha v4");
		engine.start();
	}

}
