package com.brona.gameengine;

public class Engine implements Runnable {

	private Thread thread;
	private Window window;
	private Renderer renderer;
	private Input input;
	private GameInterface game;

	private boolean running = false;

	private int width, height;
	private float scale;
	private String title = "Brona's OpenGE Pre-alpha v4";

	private final double UPDATE_CAP = 1.0 / 60.0;

	public Engine(GameInterface game) {
		this.game = game;
	}

	public void start() {
		this.window = new Window(this);
		this.renderer = new Renderer(this);
		this.input = new Input(this);

		this.thread = new Thread(this);
		this.thread.run();
	}

	public void stop() {

	}

	public void run() {
		this.running = true;

		this.game.init(this);
		
		double thisTime = 0;
		double lastTime = System.nanoTime() / 1000000000.0;
		double passedTime = 0;
		double unprocessedTime = 0;

		double renderTime = 0;

		double frameTime = 0.0;
		int frames = 0;
		int fps = 0;

		boolean render = true;
		
		while (this.running) {

			render = false;

			thisTime = System.nanoTime() / 1000000000.0;
			passedTime = thisTime - lastTime;
			lastTime = thisTime;
			unprocessedTime += passedTime;

			renderTime += passedTime;
			frameTime += passedTime;

			while (unprocessedTime >= this.UPDATE_CAP) {
				unprocessedTime -= this.UPDATE_CAP;
				render = true;

				this.game.update(this, (float)renderTime, fps);
				this.input.update();

				renderTime = 0;

				if (frameTime >= 1.0) {
					frameTime = 0.0;
					fps = frames;
					frames = 0;
				}

			}

			if (render) {
				frames++;
				this.renderer.clear();
				this.game.render(this, this.renderer);
				this.renderer.render();
				this.window.update();
			} else {
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

		}

	}

	public void dispose() {

	}

	public int getWidth() {
		return this.width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return this.height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public float getScale() {
		return this.scale;
	}

	public void setScale(float scale) {
		this.scale = scale;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Window getWindow() {
		return this.window;
	}

	public Input getInput() {
		return this.input;
	}

	public Renderer getRenderer() {
		return this.renderer;
	}

}
