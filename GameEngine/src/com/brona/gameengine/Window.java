package com.brona.gameengine;

import java.awt.BorderLayout;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;

public class Window {

	private JFrame frame;
	private BufferedImage image;
	private Canvas canvas;
	private BufferStrategy bufferStrategy;
	private Graphics graphics;

	public Window(Engine engine) {
		this.image = new BufferedImage(engine.getWidth(), engine.getHeight(), BufferedImage.TYPE_INT_RGB);
		this.canvas = new Canvas();
		Dimension s = new Dimension((int) (engine.getWidth() * engine.getScale()),
				(int) (engine.getHeight() * engine.getScale()));
		this.canvas.setPreferredSize(s);
		this.canvas.setMinimumSize(s);
		this.canvas.setMaximumSize(s);

		this.frame = new JFrame(engine.getTitle());
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setLayout(new BorderLayout());
		this.frame.add(this.canvas, BorderLayout.CENTER);
		this.frame.pack();
		this.frame.setLocationRelativeTo(null);
		this.frame.setResizable(false);
		this.frame.setVisible(true);

		//this.frame.getContentPane().setCursor(java.awt.Toolkit.getDefaultToolkit().createCustomCursor(new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB), new java.awt.Point(0, 0), "blank cursor"));
		
		this.canvas.createBufferStrategy(2);
		this.canvas.requestFocus();
		this.bufferStrategy = this.canvas.getBufferStrategy();
		this.graphics = this.bufferStrategy.getDrawGraphics();

	}

	public void update() {
		this.graphics.drawImage(this.image, 0, 0, this.canvas.getWidth(), this.canvas.getHeight(), null);
		this.bufferStrategy.show();
	}

	public BufferedImage getImage() {
		return this.image;
	}

	public Canvas getCanvas() {
		return this.canvas;
	}
}
