package com.brona.gameengine;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

public class Input implements KeyListener, MouseListener, MouseMotionListener, MouseWheelListener {

	private Engine engine;

	private final int NUM_KEYS = 256;
	private boolean[] keys = new boolean[this.NUM_KEYS];
	private boolean[] lastKeys = new boolean[this.NUM_KEYS];

	private final int NUM_BUTTONS = 5;
	private boolean[] buttons = new boolean[this.NUM_BUTTONS];
	private boolean[] lastButtons = new boolean[this.NUM_BUTTONS];

	private int mouseX = 0, mouseY = 0;
	private int scroll = 0;

	public Input(Engine engine) {
		this.engine = engine;

		this.engine.getWindow().getCanvas().addKeyListener(this);
		this.engine.getWindow().getCanvas().addMouseListener(this);
		this.engine.getWindow().getCanvas().addMouseMotionListener(this);
		this.engine.getWindow().getCanvas().addMouseWheelListener(this);
	}

	public void update() {
		for (int i = 0; i < this.NUM_KEYS; ++i) {
			this.lastKeys[i] = this.keys[i];
		}

		for (int i = 0; i < this.NUM_BUTTONS; ++i) {
			this.lastButtons[i] = this.buttons[i];
		}

		this.scroll = 0;
	}

	public boolean isKey(int keyCode) {
		return this.keys[keyCode];
	}

	public boolean isKeyDown(int keyCode) {
		return !this.lastKeys[keyCode] && this.keys[keyCode];
	}

	public boolean isKeyUp(int keyCode) {
		return this.lastKeys[keyCode] && !this.keys[keyCode];
	}

	public boolean isButton(int buttonID) {
		return this.buttons[buttonID];
	}

	public boolean isButtonDown(int buttonID) {
		return !this.lastButtons[buttonID] && this.buttons[buttonID];
	}

	public boolean isButtonUp(int buttonID) {
		return this.lastButtons[buttonID] && !this.buttons[buttonID];
	}

	public int getMouseX() {
		return this.mouseX;
	}

	public int getMouseY() {
		return this.mouseY;
	}

	public int getScroll() {
		return this.scroll;
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() < this.keys.length)
			this.keys[e.getKeyCode()] = true;

	}

	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode() < this.keys.length)
			this.keys[e.getKeyCode()] = false;

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (e.getButton() < this.buttons.length)
			this.buttons[e.getButton()] = true;

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (e.getButton() < this.buttons.length)
			this.buttons[e.getButton()] = false;

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// Auto-generated method stub

	}

	@Override
	public void mouseDragged(MouseEvent e) {
		this.mouseMoved(e);

	}

	@Override
	public void mouseMoved(MouseEvent e) {
		this.mouseX = (int) (e.getX() / this.engine.getScale());
		this.mouseY = (int) (e.getY() / this.engine.getScale());

	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		this.scroll = e.getWheelRotation();

	}

}
