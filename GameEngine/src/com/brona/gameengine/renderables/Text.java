package com.brona.gameengine.renderables;

import com.brona.gameengine.Renderer.Renderable;
import com.brona.gameengine.renderables.Font.Letter;

public class Text extends Renderable {

	private Font font;
	private String text;
	private int color;
	private boolean isGenerated;

	public Text(String text) {
		this(text, 0xffffffff, Font.STANDARD);
	}

	public Text(String text, int color) {
		this(text, color, Font.STANDARD);
	}

	public Text(String text, Font font) {
		this(text, 0xffffffff, font);
	}

	public Text(String text, int color, Font font) {
		super();
		super.isTransparent = true;

		this.font = font;
		this.text = text;
		this.color = color;

		this.isGenerated = false;
	}

	public void setFont(Font font) {
		if(this.font != font)
		    this.isGenerated = false;
		
		this.font = font;
	}

	public void setText(String text) {
		if(this.text != text)
		    this.isGenerated = false;
		
		this.text = text;
	}

	public void setColor(int color) {
		if(this.color != color)
		    this.isGenerated = false;
		
		this.color = color;
	}

	private void generate() {
		if (((this.color >> 24) & 0xff) == 0) {
			super.width = 0;
			super.height = 0;
			super.pixels = null;
			return;
		}
		if (this.text.length() == 0) {
			super.width = 0;
			super.height = 0;
			super.pixels = null;
			return;
		}

		Letter[] letters = new Letter[this.text.length()];
		int[] offsets = new int[letters.length];
		int width = 0;
		for (int i = 0; i < letters.length; ++i) {
			letters[i] = this.font.getLetter(this.text.charAt(i));
			offsets[i] = width;
			width += letters[i].getWidth();
		}
		int[] pixels = new int[Font.FONT_HEIGHT * width];

		boolean transparentColor = ((this.color >> 24) & 0xff) != 255;
		for (int i = 0; i < letters.length; ++i) {
			int[] letterPixels = letters[i].getPixels();
			int letterWidth = letters[i].getWidth();
			for (int row = 0; row < Font.FONT_HEIGHT; ++row) {
				for (int column = 0; column < letterWidth; ++column) {
					if (transparentColor) {
						int a = ((this.color >> 24) & 0xff)
								* (((letterPixels[column + row * letterWidth]) >> 24) & 0xff);
						pixels[(offsets[i] + column) + row * width] = ((a << 24) | (this.color & 0x00ffffff));
					} else {
						pixels[(offsets[i] + column) + row * width] = (letterPixels[column + row * letterWidth]
								| (this.color & 0x00ffffff));
					}
				}
			}
		}

		super.height = Font.FONT_HEIGHT;
		super.width = width;
		super.pixels = pixels;
		this.isGenerated = true;
	}

	@Override
	public int getWidth() {
		if (!this.isGenerated)
			generate();
		return super.getWidth();
	}

	@Override
	public int getHeight() {
		if (!this.isGenerated)
			generate();
		return super.getHeight();
	}

	@Override
	public int[] getPixels() {
		if (!this.isGenerated)
			generate();
		return super.getPixels();
	}

	public Font getFont() {
		return this.font;
	}

	public String getText() {
		return this.text;
	}

	public int getColor() {
		return this.color;
	}

}
