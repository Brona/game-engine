package com.brona.engineV2.rendering;

import java.awt.image.DataBufferByte;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.ListIterator;

import com.brona.engineV2.EngineContainer;

public class Renderer {
	
	private LinkedList<RenderRequest> addRequests;
	private LinkedList<RenderRequest> modifyRequests;
	
	private LinkedList<RenderObject> objects;
	private byte[] screenPixels;
	private int screenWidth, screenHeight;
	
	
    public Renderer(EngineContainer ec) {
    	screenWidth = ec.getWidth();
		screenHeight = ec.getHeight();
		screenPixels = ((DataBufferByte) ec.getWindow().getImage().getRaster().getDataBuffer()).getData();
	
		addRequests = new LinkedList<>();
		modifyRequests = new LinkedList<>();
    
		objects = new LinkedList<>();
    }

    public void render() {
    	System.out.println(Thread.currentThread().getName() + ": RENDERER-RENDER");
    	
    	int lowestX = 0, lowestY = 0, highestX = 0, highestY = 0;
    	
    	if (modifyRequests.size() > 0) {
    	    ListIterator<RenderObject> objectIterator = objects.listIterator(0);
    	    RenderObject currentObject;
    	    ListIterator<RenderRequest> requestIterator = modifyRequests.listIterator(0);
			RenderRequest currentRequest;
    	    
    	    modifyRequests.sort(new Comparator<RenderRequest>() {
    			@Override
    			public int compare(RenderRequest r1, RenderRequest r2) {
    				return (((r1.getOld().getLayer() > r2.getOld().getLayer()) ? 1 : 0 ) - ((r1.getOld().getLayer() < r2.getOld().getLayer()) ? 1 : 0 ));
    			}
        	});
    	    objects.sort(new Comparator<RenderObject>() {
    			@Override
    			public int compare(RenderObject o1, RenderObject o2) {
    				return (((o1.getLayer() > o2.getLayer()) ? 1 : 0 ) - ((o1.getLayer() < o2.getLayer()) ? 1 : 0 ));
    			}
        	});
    	    
    	    while(objectIterator.hasNext() && requestIterator.hasNext()) {
    	        currentObject = objectIterator.next();
                while(requestIterator.hasNext() && (currentObject.getLayer() == (currentRequest = requestIterator.next()).getOld().getLayer())) {
    			    if(currentObject == currentRequest.getOld()) {
    			    	if(currentRequest.getType() == RenderRequest.REQUEST_TYPE_UPDATE) {
    			    		objectIterator.set(currentRequest.getUpdated());    			    		
    			    	} else {
    			    		objectIterator.remove();
    			    	}
    			    }
    		    }
    		}
    	}
    
    	while(addRequests.size() > 0) {
    		objects.add(addRequests.removeFirst().getUpdated());
		}
    
    	objects.sort(new Comparator<RenderObject>() {
			@Override
			public int compare(RenderObject o1, RenderObject o2) {
				return (((o1.getLayer() > o2.getLayer()) ? 1 : 0 ) - ((o1.getLayer() < o2.getLayer()) ? 1 : 0 ));
			}
    	});
    
    	ListIterator<RenderObject> iterator = objects.listIterator(0);
	    RenderObject object;
	    
	    while(iterator.hasNext()) {
	    	
	    }
    	
	    
    }
    
    public void addRequest(RenderRequest request) {
        if(request.getType() == RenderRequest.REQUEST_TYPE_ADD) {
        	addRequests.add(request);
        } else {
        	modifyRequests.add(request);
        }
    }

}
