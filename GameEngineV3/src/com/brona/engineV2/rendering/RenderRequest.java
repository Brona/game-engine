package com.brona.engineV2.rendering;

public class RenderRequest {
	
	public static final int REQUEST_TYPE_ADD    = 0;
	public static final int REQUEST_TYPE_UPDATE = 1; 
	public static final int REQUEST_TYPE_REMOVE = 2; 
	
	private int type;
	private RenderObject old;
	private RenderObject updated;
	
	public RenderRequest(Renderable renderable, int offsetX, int offsetY, int layer) {
		this.type = REQUEST_TYPE_ADD;
		this.old = null;
		this.updated = new RenderObject(renderable, offsetX, offsetY, layer);
	}	
	
    public RenderRequest updateRenderable(Renderable renderable) {
    	if(type == REQUEST_TYPE_REMOVE) throw new java.lang.IllegalStateException();
    	
    	old.copy(updated);
    	updated.setRenderable(renderable);
    	type = REQUEST_TYPE_UPDATE;
    	return this;
	}
    
    public RenderRequest updateOffsets(int offsetX, int offsetY) {
    	if(type == REQUEST_TYPE_REMOVE) throw new java.lang.IllegalStateException();
    	
    	old.copy(updated);
    	updated.setOffsetX(offsetX);
    	updated.setOffsetY(offsetY);
    	type = REQUEST_TYPE_UPDATE;
    	return this;
	}
    
    public RenderRequest updateLayer(int layer) {
        if(type == REQUEST_TYPE_REMOVE) throw new java.lang.IllegalStateException();
    	
    	old.copy(updated);
    	updated.setLayer(layer);
    	type = REQUEST_TYPE_UPDATE;
    	return this;
	}
	
    public void remove() {
    	old = updated;
    	type = REQUEST_TYPE_REMOVE;
    }

	int getType() {
		return type;
	}

	RenderObject getOld() {
		return old;
	}

	RenderObject getUpdated() {
		return updated;
	}
}
