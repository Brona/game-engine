package com.brona.engineV2;

import java.util.concurrent.Semaphore;

import com.brona.engineV2.rendering.Renderer;

public class RenderThread extends Thread {

    private Semaphore semaphore;
	
	private EngineContainer ec;
	private Renderer renderer;
	private Window window;
	
	public RenderThread(EngineContainer ec) {
		super("RenderThread");
		this.ec = ec;
		semaphore = ec.getSemaphore();
		renderer = ec.getRenderer();
		window = ec.getWindow();
	}
	
	@Override
	public void run() {
		while(ec.isRunning()) {
			try {
				semaphore.acquire(2);
			} catch (InterruptedException e) {
				e.printStackTrace();
				continue;
			}
		
		    renderer.render();    
			window.update();
		    semaphore.release(1);
		}
	}
	
}
