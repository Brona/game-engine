package com.brona.engineV2;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

public class Input implements KeyListener, MouseListener, MouseMotionListener, MouseWheelListener {

	private EngineContainer ec;

	private final int NUM_KEYS = 256;
	private boolean[] keys = new boolean[this.NUM_KEYS];
	private boolean[] lastKeys = new boolean[this.NUM_KEYS];

	private final int NUM_BUTTONS = 5;
	private boolean[] buttons = new boolean[this.NUM_BUTTONS];
	private boolean[] lastButtons = new boolean[this.NUM_BUTTONS];

	private int mouseX = 0, mouseY = 0;
	private int scroll = 0;

	public Input(EngineContainer ec) {
		this.ec = ec;

		this.ec.getWindow().getCanvas().addKeyListener(this);
		this.ec.getWindow().getCanvas().addMouseListener(this);
		this.ec.getWindow().getCanvas().addMouseMotionListener(this);
		this.ec.getWindow().getCanvas().addMouseWheelListener(this);
	}

	public void update() {
		for (int i = 0; i < this.NUM_KEYS; ++i) {
			lastKeys[i] = this.keys[i];
		}

		for (int i = 0; i < this.NUM_BUTTONS; ++i) {
			lastButtons[i] = this.buttons[i];
		}

		scroll = 0;
	}

	public boolean isKey(int keyCode) {
		return keys[keyCode];
	}

	public boolean isKeyDown(int keyCode) {
		return !lastKeys[keyCode] && keys[keyCode];
	}

	public boolean isKeyUp(int keyCode) {
		return lastKeys[keyCode] && !keys[keyCode];
	}

	public boolean isButton(int buttonID) {
		return buttons[buttonID];
	}

	public boolean isButtonDown(int buttonID) {
		return !lastButtons[buttonID] && buttons[buttonID];
	}

	public boolean isButtonUp(int buttonID) {
		return lastButtons[buttonID] && !buttons[buttonID];
	}

	public int getMouseX() {
		return mouseX;
	}

	public int getMouseY() {
		return mouseY;
	}

	public int getScroll() {
		return scroll;
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() < keys.length)
			keys[e.getKeyCode()] = true;

	}

	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode() < keys.length)
			keys[e.getKeyCode()] = false;

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (e.getButton() < buttons.length)
			buttons[e.getButton()] = true;

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (e.getButton() < buttons.length)
			buttons[e.getButton()] = false;

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// Auto-generated method stub

	}

	@Override
	public void mouseDragged(MouseEvent e) {
		mouseMoved(e);

	}

	@Override
	public void mouseMoved(MouseEvent e) {
		mouseX = (int) (e.getX() / ec.getScale());
		mouseY = (int) (e.getY() / ec.getScale());

	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		scroll = e.getWheelRotation();

	}

}
