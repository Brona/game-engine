package com.brona.engineV2;

import java.util.concurrent.Semaphore;

import com.brona.engineV2.rendering.Renderer;

public class UpdateThread extends Thread {

	private Semaphore semaphore;
	
	private EngineContainer ec;
	private GameInterface game;
    private Renderer renderer;
    private Input input;
	
	public UpdateThread(EngineContainer ec) {
		super("UpdateThread");
		this.ec = ec;
		semaphore = ec.getSemaphore();
		game = ec.getGame();
		renderer = ec.getRenderer();
		input = ec.getInput();
	}
	
	@Override
	public void run() {
		
		long lastUpdate = System.nanoTime();
		
		while(ec.isRunning()) {			
			if(lastUpdate + ec.getUpdateCap() <= System.nanoTime()) {				
				if(semaphore.drainPermits() > 0) {
				    game.render(renderer);
				    semaphore.release(2);
			    }
			    
			    game.update(input, ((System.nanoTime() - lastUpdate) / 1e9));
			    input.update();
			    lastUpdate += ec.getUpdateCap();
			} else {
				try {
					Thread.sleep((long)(((lastUpdate + ec.getUpdateCap() - System.nanoTime()) / 1e9)));
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
}
