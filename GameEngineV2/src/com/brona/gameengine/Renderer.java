package com.brona.gameengine;

import java.awt.image.DataBufferInt;

public class Renderer {

	public static final int UPPERMOST_LAYER = 31;

	private int screenWidth, screenHeight;
	private int[] screenPixels;
	
	private Camera camera;
	
	private int[] uppermostSolidPixels, uppermostPixels;
	private int[][] pixelLayers;

	public Renderer(Engine engine) {
		this.screenWidth = engine.getWidth();
		this.screenHeight = engine.getHeight();
		this.screenPixels = ((DataBufferInt) engine.getWindow().getImage().getRaster().getDataBuffer()).getData();
		
		this.uppermostPixels = new int[this.screenPixels.length];
		this.uppermostSolidPixels = new int[this.screenPixels.length];
		this.pixelLayers = new int[UPPERMOST_LAYER + 1][this.screenPixels.length];
		
		this.camera = new Camera();
	}

	public void clear() {
		for (int i = 0; i < this.screenPixels.length; ++i) {
			this.uppermostPixels[i] = 0;
			this.uppermostSolidPixels[i] = 0;
			this.pixelLayers[0][i] = 0xff000000;

			for (int j = 1; j <= UPPERMOST_LAYER; ++j) {
				this.pixelLayers[j][i] = 0x00000000;
			}
		}
	}

	public void add(Renderable renderable, int offsetX, int offsetY, int layerNumber) {
		addStatical(renderable, offsetX - this.camera.getOffsetX(), offsetY - this.camera.getOffsetY(), layerNumber);
	}
	
	
	public void addStatical(Renderable renderable, int offsetX, int offsetY, int layerNumber) {

		if ((layerNumber > UPPERMOST_LAYER) || (layerNumber < 0))
			return;

		if (renderable.getWidth() <= 0)
			return;
		if (renderable.getHeight() <= 0)
			return;
		
		if (offsetX < -renderable.getWidth())
			return;
		if (offsetY < -renderable.getHeight())
			return;
		if (offsetX >= this.screenWidth)
			return;
		if (offsetY >= this.screenHeight)
			return;

		int newX = 0;
		int newY = 0;
		int newWidth = renderable.getWidth();
		int newHeight = renderable.getHeight();

		if (offsetX < 0)
			newX = -offsetX;
		if (offsetY < 0)
			newY = -offsetY;
		if ((newWidth + offsetX) >= this.screenWidth)
			newWidth -= newWidth + offsetX - this.screenWidth;
		if ((newHeight + offsetY) >= this.screenHeight)
			newHeight -= newHeight + offsetY - this.screenHeight;

		int[] pixels = renderable.getPixels();
		int[] layer = this.pixelLayers[layerNumber];

		int screenPixelIndex = 0;

		if (renderable.isTransparent()) {

			int renderablePixelIndex = 0;

			for (int y = newY; y < newHeight; ++y) {
				for (int x = newX; x < newWidth; ++x) {

					renderablePixelIndex = x + y * renderable.getWidth();

					if ((((pixels[renderablePixelIndex]) >> 24) & 0xff) == 0)
						continue;

					screenPixelIndex = (x + offsetX) + (y + offsetY) * this.screenWidth;

					if (this.uppermostSolidPixels[screenPixelIndex] <= layerNumber) {
						if ((((pixels[renderablePixelIndex]) >> 24) & 0xff) == 255) {
							this.uppermostSolidPixels[screenPixelIndex] = layerNumber;
							if (this.uppermostPixels[screenPixelIndex] < layerNumber)
								this.uppermostPixels[screenPixelIndex] = layerNumber;
							layer[screenPixelIndex] = pixels[renderablePixelIndex];
						} else {
							if (this.uppermostPixels[screenPixelIndex] < layerNumber) {
								this.uppermostPixels[screenPixelIndex] = layerNumber;
								layer[screenPixelIndex] = pixels[renderablePixelIndex];
							} else if ((((layer[screenPixelIndex]) >> 24) & 0xff) == 0) {
								layer[screenPixelIndex] = pixels[renderablePixelIndex];
							} else if ((((layer[screenPixelIndex]) >> 24) & 0xff) == 255) {

								int cf = pixels[renderablePixelIndex];
								int cb = layer[screenPixelIndex];

								if (cb == (cf | 0xff000000))
									layer[screenPixelIndex] = cb;

								int af = ((cf >> 24) & 0xff);

								int r = ((((cf >> 16) & 0xff) * af) + (((cb >> 16) & 0xff) * (255 - af))) / 255;
								int g = ((((cf >> 8) & 0xff) * af) + (((cb >> 8) & 0xff) * (255 - af))) / 255;
								int b = (((cf & 0xff) * af) + ((cb & 0xff) * (255 - af))) / 255;

								layer[screenPixelIndex] = (0xff000000 | (r << 16) | (g << 8) | b);
							} else {
								int cf = pixels[renderablePixelIndex];
								int cb = layer[screenPixelIndex];

								int af = ((cf >> 24) & 0xff);
								int ab = ((cb >> 24) & 0xff);
								int a = (af * 255) + (ab * (255 - af));

								if ((cb | 0xff000000) == (cf | 0xff000000))
									layer[screenPixelIndex] = ((cb & 0x00ffffff) | ((a / 255) << 24));

								int r = ((((cf >> 16) & 0xff) * af * 255) + (((cb >> 16) & 0xff) * ab * (255 - af)))
										/ a;
								int g = ((((cf >> 8) & 0xff) * af * 255) + (((cb >> 8) & 0xff) * ab * (255 - af))) / a;
								int b = (((cf & 0xff) * af * 255) + ((cb & 0xff) * ab * (255 - af))) / a;

								layer[screenPixelIndex] = (((a / 255) << 24) | (r << 16) | (g << 8) | b);
							}
						}
					}

				}
			}
		} else {
			for (int y = newY; y < newHeight; ++y) {
				for (int x = newX; x < newWidth; ++x) {
					screenPixelIndex = (x + offsetX) + (y + offsetY) * this.screenWidth;
					if (this.uppermostSolidPixels[screenPixelIndex] <= layerNumber) {
						this.uppermostSolidPixels[screenPixelIndex] = layerNumber;
						if (this.uppermostPixels[screenPixelIndex] < layerNumber)
							this.uppermostPixels[screenPixelIndex] = layerNumber;
						layer[screenPixelIndex] = (pixels[x + y * renderable.getWidth()] | 0xff000000);
					}
				}
			}
		}

	}

	public void render() {
		for (int pixel = 0; pixel < this.screenWidth * this.screenHeight; ++pixel) {
			this.screenPixels[pixel] = this.pixelLayers[this.uppermostSolidPixels[pixel]][pixel];
			if (this.uppermostSolidPixels[pixel] != this.uppermostPixels[pixel]) {
				for (int layer = this.uppermostSolidPixels[pixel] + 1; layer <= this.uppermostPixels[pixel]; layer++) {
					if ((((this.pixelLayers[layer][pixel]) >> 24) & 0xff) == 0)
						continue;

					int cf = this.pixelLayers[layer][pixel];
					int cb = this.screenPixels[pixel];

					if (cb == (cf | 0xff000000))
						continue;

					int af = ((cf >> 24) & 0xff);

					int r = ((((cf >> 16) & 0xff) * af) + (((cb >> 16) & 0xff) * (255 - af))) / 255;
					int g = ((((cf >> 8) & 0xff) * af) + (((cb >> 8) & 0xff) * (255 - af))) / 255;
					int b = (((cf & 0xff) * af) + ((cb & 0xff) * (255 - af))) / 255;

					this.screenPixels[pixel] = (0xff000000 | (r << 16) | (g << 8) | b);
				}
			}
		}
	}

	public static class Renderable {		
		
		protected int width, height;
		protected int[] pixels;
		protected boolean isTransparent;

		public Renderable() {
			this.width = 0;
			this.height = 0;
			this.pixels = null;
			this.isTransparent = false;
		}
		
		public Renderable(Renderable origin) {
			this.width = origin.getWidth();
			this.height = origin.getHeight();
			this.pixels = origin.getPixels().clone();
			this.isTransparent = origin.isTransparent();
		}
		

		public int getWidth() {
			return this.width;
		}

		public int getHeight() {
			return this.height;
		}

		public int[] getPixels() {
			return this.pixels;
		}

		public boolean isTransparent() {
			return this.isTransparent;
		}

		public static final Renderable rotate(Renderable original, int degrees) {
			Renderable rotated = new Renderable();
			int[] originalPixels;
			int[] rotatedPixels;
			
			switch(degrees) {
			case 90:
				rotated = new Renderable();
				rotated.isTransparent = original.isTransparent();
				rotated.height = original.getWidth();
				rotated.width = original.getHeight();
				rotated.pixels = new int[rotated.width * rotated.height];
				originalPixels = original.getPixels();
				rotatedPixels = rotated.getPixels();
				for(int y = 0; y < rotated.getHeight(); ++y) {
					for(int x = 0; x < rotated.getWidth(); ++x) {
						rotatedPixels[x + y * rotated.getWidth()] = originalPixels[y + (rotated.getWidth() - 1 - x) * rotated.getWidth()];
					}
				}
				break;
			case 180:
				rotated = new Renderable(original);
				originalPixels = original.getPixels();
				rotatedPixels = rotated.getPixels();
				for(int y = 0; y < rotated.getHeight(); ++y) {
					for(int x = 0; x < rotated.getWidth(); ++x) {
						rotatedPixels[x + y * rotated.getWidth()] = originalPixels[(rotated.getWidth() - 1 - x) + (rotated.getHeight() - 1 - y) * rotated.getWidth()];
					}
				}
				break;
			case 270:
				rotated = new Renderable();
				rotated.isTransparent = original.isTransparent();
				rotated.height = original.getWidth();
				rotated.width = original.getHeight();
				rotated.pixels = new int[rotated.width * rotated.height];
				originalPixels = original.getPixels();
				rotatedPixels = rotated.getPixels();
				for(int y = 0; y < rotated.getHeight(); ++y) {
					for(int x = 0; x < rotated.getWidth(); ++x) {
						rotatedPixels[x + y * rotated.getWidth()] = originalPixels[(rotated.getHeight() - 1 - y) + x * rotated.getWidth()];
					}
				}
			    break;
			default:
				rotated = new Renderable(original);
				break;				
			}
			return rotated;
		}
		
	}
	
	public static class Camera {
		
		protected int offsetX, offsetY;
		
		public Camera() {
			this.offsetX = 0;
			this.offsetY = 0;
		}
		
		public void init(Engine engine, GameInterface game) { return; }
		public void update(Engine engine, GameInterface game, float deltaTime) { return; }
		public void render(Engine engine, GameInterface game, Renderer renderer) { return; }
		
		public void setOffsetX(int offsetX) {
			this.offsetX = offsetX;
		}
		
		public void setOffsetY(int offsetY) {
			this.offsetY = offsetY;
		}
		
		public int getOffsetX() {
			return this.offsetX;
		}
		
		public int getOffsetY() {
			return this.offsetY;
		}
	}

	public Camera getCamera() {
		return this.camera;
	}

	public void setCamera(Camera camera) {
		this.camera = camera;
	}

	@Deprecated
	public void clearScreen() {
		for (int i = 0; i < this.screenPixels.length; ++i) {
			this.screenPixels[i] = 0x00000000;
		}
	}

	@Deprecated
	public void drawRenderable(Renderable renderable, int offsetX, int offsetY) {

		if (offsetX < -renderable.getWidth())
			return;
		if (offsetY < -renderable.getHeight())
			return;
		if (offsetX >= this.screenWidth)
			return;
		if (offsetY >= this.screenHeight)
			return;

		int newX = 0;
		int newY = 0;
		int newWidth = renderable.getWidth();
		int newHeight = renderable.getHeight();

		if (offsetX < 0)
			newX = -offsetX;
		if (offsetY < 0)
			newY = -offsetY;
		if ((newWidth + offsetX) >= this.screenWidth)
			newWidth -= newWidth + offsetX - this.screenWidth;
		if ((newHeight + offsetY) >= this.screenHeight)
			newHeight -= newHeight + offsetY - this.screenHeight;

		for (int y = newY; y < newHeight; ++y) {
			for (int x = newX; x < newWidth; ++x) {
				this.screenPixels[(x + offsetX) + (y + offsetY) * this.screenWidth] = renderable.getPixels()[x
						+ y * renderable.getWidth()];
			}
		}
	}
}
