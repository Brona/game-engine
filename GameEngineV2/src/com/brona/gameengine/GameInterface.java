package com.brona.gameengine;

public interface GameInterface {

	public void init(Engine engine);
	
	public void update(Input input, float deltaTime);

	public void render(Renderer renderer);

}
