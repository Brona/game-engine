package com.brona.gameengine.renderables;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Font {

	public static final Font STANDARD = new Font("/fonts/standard.png");

	public static final int FONT_HEIGHT = 5;
	public static final int FONT_UNICODE_START = 32;
	public static final int FONT_UNICODE_END = 90;

	private Letter[] letters;

	public Font(String path) {

		BufferedImage image = null;
		try {
			image = ImageIO.read(Font.class.getResourceAsStream(path));
		} catch (IOException e) {
			e.printStackTrace();
		}

		int width = image.getWidth();
		int height = image.getHeight();
		int[] pixels = image.getRGB(0, 0, width, height, null, 0, width);

		image.flush();

		this.letters = new Letter[(FONT_UNICODE_END - FONT_UNICODE_START) + 2];
		int offset = 0;
		for (int letter = 0; letter < this.letters.length; ++letter) {
			int start = 0;
			int end = 0;

			while ((pixels[offset] & 0x000000ff) != 0x000000ff)
				++offset;
			start = offset;
			while ((pixels[offset] & 0x0000ff00) != 0x0000ff00)
				++offset;
			end = offset;
            offset++;
			
			int letterWidth = end - start + 1;
			int[] letterPixels = new int[FONT_HEIGHT * letterWidth];

			for (int row = 0; row < FONT_HEIGHT; ++row) {
				for (int column = 0; column < letterWidth; ++column) {
					letterPixels[column
							+ row * letterWidth] = (((pixels[(start + column) + row * width]) & 0x00ff0000) << 8);
				}
			}

			this.letters[letter] = new Letter(letterWidth, letterPixels);
		}
	}

	public Letter getLetter(char letter) {
		int unicode = Character.toUpperCase(letter);
		if ((unicode < FONT_UNICODE_START) || (unicode > FONT_UNICODE_END)) {
			unicode = FONT_UNICODE_END + 1;
		}

		unicode -= FONT_UNICODE_START;

		return this.letters[unicode];
	}

	public static class Letter {
		private int width;
		private int[] pixels;

		public Letter(int width, int[] pixels) {
			this.width = width;
			this.pixels = pixels;
		}

		public int getWidth() {
			return this.width;
		}

		public int[] getPixels() {
			return this.pixels;
		}

	}
}
