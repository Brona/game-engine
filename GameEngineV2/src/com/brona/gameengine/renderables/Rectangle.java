package com.brona.gameengine.renderables;

import com.brona.gameengine.Renderer.Renderable;

public class Rectangle extends Renderable {
	
	public Rectangle(int width, int height, int color, boolean filled) {
		super.width = width;
		super.height = height;
		
		super.isTransparent = filled ? (((color >> 24) & 0xff) != 255) : true;
		
		super.pixels =  new int[super.width * super.height];
		
		if(filled) {
			for(int i = 0; i < super.pixels.length; ++i) {
				super.pixels[i] = color;
			}
		} else {
			for(int y = 0; y < super.height; ++y) {
				for(int x = 0; x < super.width; ++x) {
					if((x == 0) || (y == 0) || (x == (super.width - 1)) || (y == (super.height - 1))) {
						super.pixels[x + y * super.width] = color;
					} else {
					    super.pixels[x + y * super.width] = 0x00000000;	
					}
				}
			}			
		}
		
	}
	
}
