package com.brona.gameengine;

import java.util.LinkedList;
import java.util.ListIterator;

public class Engine implements Runnable {

	private long updateCap = Math.round((1.0 / 60.0) * 1e9); //In nanoseconds 60fps
	private boolean running = false;
	
	private Thread thread;
	private Window window;
	private Renderer renderer;
	private Input input;
	private GameInterface game;

	private int width, height;
	private float scale;
	private String title = "Brona's OpenGE Patch-6";

	private LinkedList<Timer> timers;
	
	public Engine(GameInterface game) {
		this.game = game;
		timers = new LinkedList<>();
	}

	public void start() {
		if(running) return;
		this.running = true;
		
		window = new Window(this);
		renderer = new Renderer(this);
		input = new Input(this);

		thread = new Thread(this, "Thread-Main");
		thread.run();
	}

	public void stop() {
		if(!running) return;
        running = false;
        System.err.println("Exiting game");
	}

	public void run() {
        this.game.init(this);
		
        long lastUpdate = System.nanoTime();
		
		while(running) {			
			if(lastUpdate + updateCap > System.nanoTime()) {				
				try {
					Thread.sleep((long)(((lastUpdate + updateCap - System.nanoTime()) / 1e9)));
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} else {
				while(lastUpdate + updateCap <= System.nanoTime()) {
					if(timers.size() > 0) {
					    ListIterator<Timer> iterator = timers.listIterator(0);
						for(int i = 0; i < timers.size(); ++i) {
						    if(iterator.next().update()) {
						    	iterator.remove();
						    	--i;
						    }
					    }
					}
					game.update(input, (float) ((System.nanoTime() - lastUpdate) / 1e9));
					this.input.update();
				    lastUpdate += updateCap;
				}
				renderer.clear();
				game.render(renderer);
				renderer.render();
				window.update();
			}
		}
		System.exit(0);
	}

	public void dispose() {

	}

	public Timer setTimer(long duration, Runnable callback) {
		Timer t = new Timer(duration, callback);
		timers.add(t);
		return t;
	}
	
	public boolean isRunning() {
		return running;
	}
	
	public GameInterface getGame() {
		return game;
	}
	
	public Window getWindow() {
		return window;
	}

	public Input getInput() {
		return input;
	}

	public Renderer getRenderer() {
		return renderer;
	}
	
	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public float getScale() {
		return scale;
	}

	public void setScale(float scale) {
		this.scale = scale;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public long getUpdateCap() {
		return updateCap;
	}

	public void setUpdateCap(long updateCap) {
		this.updateCap = updateCap;
	}
	
	public void setFps(int fps) {
		this.updateCap = Math.round((1.0 / fps) * 1e9);
	}

}
