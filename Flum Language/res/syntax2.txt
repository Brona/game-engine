classname:
    One big letter, then letters and numbers

varname:
    One small letter, then letters and numbers

type:
    can be a <classname> or some primitive type such as int dec char byte bool func

value:
    a value of some variable

code:
    sequence of other commands

reachable prefixes:
    such as public or private

package keyword declaration:

use declaration:
    'use <package>.<classname>'

class declaration:
    'class <classname> [extends <classname>] { <code> }'

library declaration:
    'library <classname> [extends <classname>] { <code> }'

inside class/library usable code {

    variable declaration:
    '<reachable prefixes> <type> <varname> [= <value>]'

    func declaration (instance of value):
    '(<type> <varname>[, <type> <varname>[...]]) -> <type> { <code> }'

    hardcoded variables(instance of value) *

    variable getting(instance of value):
        <varname>
      OR
        <varname>.<varname>
      OR
        <libraryName>.<varname>

    function call(instance of value):
        '<value>([<value>[, <value>[...]]])'

}

inside func usable code {
    variable declaration:
        '<type> <varname> [= <value>]'

    func declaration (instance of value):
        '([<type> <varname>[, <type> <varname>[...]]]) -> <type> { <code> }'

    hardcoded variables(instance of value) *

    variable getting(instance of value):
            <varname>
          OR
            <varname>.<varname>
          OR
            <libraryName>.<varname>

    function call(instance of value):
        '<value>([<value>[, <value>[...]]])'

    variable assignment:
        '<varname> = <value>'

    if statement:
        'if(<value>) { <code> } [<else statement>]'

    else statement:
        'else { <code> }'
      OR
        'elseif(<value>) { <code> } [<else statement>]'

    for statement:
        'for(<variable declaration: type int>, <value>) { <code> }' - until variable declared is equal to value, the loop runs, the value is incrementer each time.

    while statement:
        'while(<value>) { <code> }'

    return statement:
        'return(<value>)' - there are no brackets when the function returns void



}