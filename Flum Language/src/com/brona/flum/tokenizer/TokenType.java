package com.brona.flum.tokenizer;

public enum TokenType {

    EMPTY,

    TOKEN, // + - * / , .

    IDENTIFIER,

    BOOL_LITERAL,

    INT_LITERAL,

    DEC_LITERAL,

    STRING_LITERAL

}
