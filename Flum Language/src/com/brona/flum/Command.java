package com.brona.flum;

public abstract class Command {

    protected Block superBlock;

    public Command(Block superBlock) {
        this.superBlock = superBlock;
    }

    public Block getSuperBlock() {
        return superBlock;
    }

}
